//! Command-line frontend for SOP.
//!
//! This is an implementation of the SOP Command-Line protocol in
//! terms of the trait [`crate::SOP`], hence it can be shared between
//! SOP implementations.
//!
//! To use, add this snippet to `Cargo.toml`:
//!
//! ```toml
//! [[bin]]
//! path = "src/main.rs"
//! required-features = ["cli"]
//!
//! [features]
//! cli = ["sop/cli"]
//! ```
//!
//! And create `src/main.rs` along the lines of:
//!
//! ```rust,ignore
//! fn main() {
//!     sop::cli::main(&MySOPImplementation::default());
//! }
//! ```
//!
//! ## Generating shell completions
//!
//! To create shell completions, add this snippet to `Cargo.toml`:
//!
//! ```toml
//! [build-dependencies]
//! sop = "..."
//! ```
//!
//! And create `build.rs` along the lines of:
//!
//! ```rust,no_run
//! #[cfg(feature = "cli")]
//! fn main() {
//!     let outdir = std::env::var_os("CARGO_TARGET_DIR")
//!         .or(std::env::var_os("OUT_DIR"))
//!         .expect("cargo to set OUT_DIR");
//!     sop::cli::write_shell_completions("sqop", outdir).unwrap();
//! }
//!
//! #[cfg(not(feature = "cli"))]
//! fn main() {}
//! ```
//!
//! # Features and limitations
//!
//! - The special designator `@FD:` is only available on UNIX-like systems.
//!
//! - On Windows, certs and keys provided via the `@ENV:` special
//!   designator must be ASCII armored and well-formed UTF-8.

use std::{
    io::{self, Write},
    path::Path,
};

use anyhow::{Context, Result};

use chrono::{DateTime, NaiveTime, offset::Utc};
use clap::{CommandFactory, FromArgMatches, Parser};

use crate::{
    ArmorLabel,
    EncryptAs,
    Save,
    Load,
    InlineSignAs,
    Password,
    SignAs,
    SOP,
};

#[derive(Debug, Parser)]
#[clap(about = "An implementation of the Stateless OpenPGP Command Line Interface")]
#[clap(disable_version_flag(true))]
enum Operation {
    /// Prints version information.
    ///
    /// Invoked without arguments, returns name and version of the SOP
    /// implementation.
    #[clap(display_order = 100)]
    Version {
        /// Returns name and version of the primary underlying OpenPGP
        /// toolkit.
        #[clap(long, conflicts_with("extended"), conflicts_with("sop_spec"))]
        backend: bool,

        /// Returns multiple lines of name and version information.
        ///
        /// The first line is the name and version of the SOP
        /// implementation, but the rest have no defined structure.
        #[clap(long, conflicts_with("backend"), conflicts_with("sop_spec"))]
        extended: bool,

        /// Returns the latest version of the SOP spec that is
        /// implemented.
        #[clap(long, conflicts_with("backend"), conflicts_with("extended"))]
        sop_spec: bool,
    },
    /// Emits a list of profiles supported by the identified
    /// subcommand.
    #[clap(display_order = 150)]
    ListProfiles {
        subcommand: String,
    },
    /// Generates a Secret Key.
    #[clap(display_order = 200)]
    GenerateKey {
        /// Don't ASCII-armor output.
        #[clap(long)]
        no_armor: bool,
        /// Select the profile to use for key generation.
        #[clap(long)]
        profile: Option<String>,
        /// Create a signing-only key.
        #[clap(long)]
        signing_only: bool,
        /// Protect the newly generated key with the given password.
        #[clap(long)]
        with_key_password: Option<String>,
        /// UserIDs for the generated key.
        userids: Vec<String>,
    },

    /// Updates a key's password.
    ///
    /// The output will be the same set of OpenPGP Transferable Secret
    /// Keys as the input, but with all secret key material locked
    /// according to the password indicated by the
    /// `--new-key-password` option (or, with no password at all, if
    /// `--new-key-password` is absent).  Note that
    /// `--old-key-password` can be supplied multiple times, and each
    /// supplied password will be tried as a means to unlock any
    /// locked key material encountered.  It will normalize a
    /// Transferable Secret Key to use a single password even if it
    /// originally had distinct passwords locking each of the subkeys.
    ///
    /// If any secret key packet is locked but cannot be unlocked with
    /// any of the supplied `--old-key-password` arguments, this
    /// subcommand should fail with `KEY_IS_PROTECTED`.
    #[clap(display_order = 210)]
    ChangeKeyPassword {
        /// Don't ASCII-armor output.
        #[clap(long)]
        no_armor: bool,
        /// The new password to lock the key with, or just unlock the
        /// key if the option is absent.
        #[clap(long)]
        new_key_password: Option<String>,
        /// Unlock the keys with these passwords.
        #[clap(long, number_of_values = 1)]
        old_key_password: Vec<String>,
    },

    /// Creates a Revocation Certificate.
    ///
    /// Generate a revocation certificate for each Transferable Secret
    /// Key found.
    #[clap(display_order = 220)]
    RevokeKey {
        /// Don't ASCII-armor output.
        #[clap(long)]
        no_armor: bool,
        /// Unlock the keys with these passwords.
        #[clap(long, number_of_values = 1)]
        with_key_password: Vec<String>,
    },

    /// Extracts a Certificate from a Secret Key.
    #[clap(display_order = 300)]
    ExtractCert {
        /// Don't ASCII-armor output.
        #[clap(long)]
        no_armor: bool,
    },
    /// Creates Detached Signatures.
    #[clap(display_order = 400)]
    Sign {
        /// Don't ASCII-armor output.
        #[clap(long)]
        no_armor: bool,
        /// Sign binary data or UTF-8 text.
        #[clap(default_value = "binary", long = "as")]
        as_: SignAs,
        /// Emit the digest algorithm used to the specified file.
        #[clap(long)]
        micalg_out: Option<String>,
        /// Try to decrypt the signing KEYS with these passwords.
        #[clap(long, number_of_values = 1)]
        with_key_password: Vec<String>,
        /// Keys for signing.
        keys: Vec<String>,
    },
    /// Verifies Detached Signatures.
    #[clap(display_order = 500)]
    Verify {
        /// Consider signatures before this date invalid.
        #[clap(long)]
        not_before: Option<NotBefore>,
        /// Consider signatures after this date invalid.
        #[clap(long)]
        not_after: Option<NotAfter>,
        /// Signatures to verify.
        signatures: String,
        /// Certs for verification.
        certs: Vec<String>,
    },
    /// Encrypts a Message.
    #[clap(display_order = 600)]
    Encrypt {
        /// Don't ASCII-armor output.
        #[clap(long)]
        no_armor: bool,
        /// Select the profile to use for encryption.
        #[clap(long)]
        profile: Option<String>,
        /// Encrypt binary data or UTF-8 text.
        #[clap(default_value = "binary", long = "as")]
        as_: EncryptAs,
        /// Encrypt with passwords.
        #[clap(long, number_of_values = 1)]
        with_password: Vec<String>,
        /// Keys for signing.
        #[clap(long, number_of_values = 1)]
        sign_with: Vec<String>,
        /// Try to decrypt the signing KEYS with these passwords.
        #[clap(long, number_of_values = 1)]
        with_key_password: Vec<String>,
        /// Write the session key here.
        #[clap(long)]
        session_key_out: Option<String>,
        /// Encrypt for these certs.
        certs: Vec<String>,
    },
    /// Decrypts a Message.
    #[clap(display_order = 700)]
    Decrypt {
        /// Write the session key here.
        #[clap(long)]
        session_key_out: Option<String>,
        /// Try to decrypt with this session key.
        #[clap(long, number_of_values = 1)]
        with_session_key: Vec<String>,
        /// Try to decrypt with this password.
        #[clap(long, number_of_values = 1)]
        with_password: Vec<String>,
        /// Write verification result here.
        #[clap(long, alias("verify-out"))]
        verifications_out: Option<String>,
        /// Certs for verification.
        #[clap(long, number_of_values = 1)]
        verify_with: Vec<String>,
        /// Consider signatures before this date invalid.
        #[clap(long)]
        verify_not_before: Option<NotBefore>,
        /// Consider signatures after this date invalid.
        #[clap(long)]
        verify_not_after: Option<NotAfter>,
        /// Try to decrypt the encryption KEYS with these passwords.
        #[clap(long, number_of_values = 1)]
        with_key_password: Vec<String>,
        /// Try to decrypt with these keys.
        keys: Vec<String>,
    },
    /// Converts binary OpenPGP data to ASCII.
    #[clap(display_order = 800)]
    Armor {
        /// Indicates the kind of data.
        #[clap(long, default_value = "auto", hide(true))]
        label: ArmorLabel,
    },
    /// Converts ASCII OpenPGP data to binary.
    #[clap(display_order = 900)]
    Dearmor {
    },
    /// Splits Signatures from an Inline-Signed Message.
    #[clap(display_order = 750)]
    InlineDetach {
        /// Don't ASCII-armor the signatures.
        #[clap(long)]
        no_armor: bool,
        /// Write Signatures here.
        #[clap(long)]
        signatures_out: String,
    },
    /// Verifies Inline-Signed Messages.
    #[clap(display_order = 751)]
    InlineVerify {
        /// Consider signatures before this date invalid.
        #[clap(long)]
        not_before: Option<NotBefore>,
        /// Consider signatures after this date invalid.
        #[clap(long)]
        not_after: Option<NotAfter>,
        /// Write verification result here.
        #[clap(long)]
        verifications_out: Option<String>,
        /// Certs for verification.
        certs: Vec<String>,
    },
    /// Creates Inline-Signed Messages.
    #[clap(display_order = 752)]
    InlineSign {
        /// Don't ASCII-armor output.
        #[clap(long)]
        no_armor: bool,
        /// Sign binary data, UTF-8 text, or using the Cleartext
        /// Signature Framework.
        #[clap(default_value = "binary", long = "as")]
        as_: InlineSignAs,
        /// Try to decrypt the signing KEYS with these passwords.
        #[clap(long, number_of_values = 1)]
        with_key_password: Vec<String>,
        /// Keys for signing.
        keys: Vec<String>,
    },
    /// Unsupported subcommand.
    #[clap(external_subcommand)]
    Unsupported(Vec<String>),
}

/// Generates shell completions.
pub fn write_shell_completions<B, P>(binary_name: B,
                                     out_path: P)
                                     -> io::Result<()>
where B: AsRef<str>,
      P: AsRef<Path>,
{
    use clap_complete::{generate_to, Shell};

    let binary_name = binary_name.as_ref();
    let out_path = out_path.as_ref();
    std::fs::create_dir_all(&out_path)?;

    let mut clap = Operation::command();
    for shell in &[Shell::Bash, Shell::Fish, Shell::Zsh, Shell::PowerShell,
                   Shell::Elvish] {
        let path = generate_to(*shell, &mut clap, binary_name, out_path)?;
        println!("cargo:warning=completion file is generated: {:?}", path);
    }
    Ok(())
}

/// Implements the SOP command-line interface.
pub fn main<'s, S, C, K, Sigs>(sop: &'s S) -> !
where
    S: SOP<'s, Certs = C, Keys = K, Sigs = Sigs>,
    C: Load<'s, S> + Save,
    K: Load<'s, S> + Save,
    Sigs: Load<'s, S> + Save,
{
    use std::process::exit;

    match real_main::<S, C, K, Sigs>(sop) {
        Ok(()) => exit(0),
        Err(e) => {
            print_error_chain(&e);
            let e = match e.downcast::<crate::Error>() {
                Ok(e) => exit(Error::from(e).into()),
                Err(e) => e,
            };
            let e = match e.downcast::<Error>() {
                Ok(e) => exit(e.into()),
                Err(e) => e,
            };

            // XXX: Would be nice to at least inform developers that
            // we return a generic error here.
            let _ = e;
            exit(1);
        },
    }
}

fn real_main<'s, S, C, K, Sigs>(sop: &'s S) -> Result<()>
where
    S: SOP<'s, Certs = C, Keys = K, Sigs = Sigs>,
    C: Load<'s, S> + Save,
    K: Load<'s, S> + Save,
    Sigs: Load<'s, S> + Save,
{
    // Do a little dance to supply name and version to clap.
    let v = sop.version()?.frontend()?;
    let name = format!("SOP {}", v.name);
    let version = v.version.clone();
    let about = format!("An implementation of the Stateless OpenPGP Command \
                        Line Interface using {} {}",
                        v.name, v.version);
    let app = Operation::command()
        .name(Box::leak(name.into_boxed_str()) as &str)
        .about(Box::leak(about.into_boxed_str()) as &str)
        .version(Box::leak(version.into_boxed_str()) as &str);

    let matches = match app.clone().try_get_matches() {
        Ok(v) => v,
        Err(e) => {
            use clap::error::ErrorKind;
            return match e.kind() {
                ErrorKind::DisplayHelp => {
                    e.exit()
                },
                ErrorKind::UnknownArgument =>
                    Err(anyhow::Error::from(Error::UnsupportedOption)
                        .context(format!("{}", e))),
                _ => Err(e.into()),
            };
        },
    };

    match Operation::from_arg_matches(&matches)? {
        Operation::Version { backend, extended, sop_spec } => {
            let version = sop.version()?;
            match (backend, extended, sop_spec) {
                (false, false, false) => {
                    let v = version.frontend()?;
                    println!("{} {}", v.name, v.version);
                },
                (true, false, false) => {
                    let v = version.backend()?;
                    println!("{} {}", v.name, v.version);
                },
                (false, true, false) => {
                    let v = version.frontend()?;
                    println!("{} {}", v.name, v.version);
                    println!("sop-rs {}", env!("CARGO_PKG_VERSION"));
                    println!("{}", version.extended()?);
                },
                (false, false, true) => {
                    println!("{}", sop.spec_version());
                },
                _ => unreachable!("flags are mutually exclusive"),
            }
        },

        Operation::ListProfiles { subcommand, } => {
            let profiles = match subcommand.as_str() {
                "generate-key" => sop.generate_key()?.list_profiles(),
                "encrypt" => sop.encrypt()?.list_profiles(),
                _ => return Err(Error::UnsupportedProfile.into()),
            };
            for (p, d) in profiles {
                println!("{}: {}", p, d);
            }
        },

        Operation::GenerateKey {
            no_armor,
            profile,
            signing_only,
            with_key_password,
            userids,
        } => {
            let mut op = sop.generate_key()?;
            if let Some(p) = profile {
                op = op.profile(&p)?;
            }
            if signing_only {
                op = op.signing_only();
            }
            if let Some(p) = with_key_password {
                op = op.with_key_password(get_password(p)?)?;
            }
            for u in userids {
                op = op.userid(&u);
            }
            op.generate()?
                .to_writer(! no_armor, &mut io::stdout())?;
        },

        Operation::ChangeKeyPassword {
            no_armor,
            new_key_password,
            old_key_password,
        } => {
            let mut op = sop.change_key_password()?;
            if let Some(p) = new_key_password {
                op = op.new_key_password(get_password(p)?)?;
            }
            for p in old_key_password {
                op = op.old_key_password(get_password(p)?)?;
            }
            op.keys(&K::from_reader(sop, &mut io::stdin())?)?
                .to_writer(! no_armor, &mut io::stdout())?;
        }

        Operation::RevokeKey { no_armor, with_key_password, } => {
            let mut op = sop.revoke_key()?;
            for p in with_key_password {
                op = op.with_key_password(get_password(p)?)?;
            }
            op.keys(&K::from_reader(sop, &mut io::stdin())?)?
                .to_writer(! no_armor, &mut io::stdout())?;
        }

        Operation::ExtractCert { no_armor, } => {
            sop.extract_cert()?
                .keys(&K::from_reader(sop, &mut io::stdin())?)?
                .to_writer(! no_armor, &mut io::stdout())?;
        },

        Operation::Sign {
            no_armor,
            as_,
            micalg_out,
            with_key_password,
            keys,
        } => {
            let mut op = sop.sign()?.mode(as_);
            for p in with_key_password {
                op = op.with_key_password(get_password_unchecked(p)?)?;
            }
            for mut stream in load_files(keys)? {
                op = op.keys(&K::from_reader(sop, &mut stream)?)?;
            }
            let (micalg, sigs) = op.data(&mut io::stdin())?;
            if let Some(path) = micalg_out {
                let mut sink = create_file(path)?;
                write!(sink, "{}", micalg)?;
            }
            sigs.to_writer(! no_armor, &mut io::stdout())?;
        },

        Operation::Verify { not_before, not_after, signatures, certs, } => {
            let mut op = sop.verify()?;
            if let Some(t) = not_before {
                op = op.not_before(t.into());
            }
            if let Some(t) = not_after {
                op = op.not_after(t.into());
            }
            for mut stream in load_files(certs)? {
                op = op.certs(&C::from_reader(sop, &mut stream)?)?;
            }
            let verifications =
                op.signatures(&Sigs::from_reader(sop, &mut load_file(signatures)?)?)?
                .data(&mut io::stdin())?;

            for v in verifications {
                println!("{}", v);
            }
        },

        Operation::Encrypt {
            no_armor,
            profile,
            as_,
            with_password,
            with_key_password,
            sign_with,
            session_key_out,
            certs,
        } =>
        {
            let session_key_out: Option<Box<dyn io::Write>> =
                if let Some(f) = session_key_out {
                    Some(Box::new(create_file(f)?))
                } else {
                    None
                };

            let mut op = sop.encrypt()?.mode(as_);
            if no_armor {
                op = op.no_armor();
            }
            if let Some(p) = profile {
                op = op.profile(&p)?;
            }
            for p in with_key_password {
                op = op.with_key_password(get_password_unchecked(p)?)?;
            }
            for mut stream in load_files(sign_with)? {
                op = op.sign_with_keys(&K::from_reader(sop, &mut stream)?)?;
            }
            for pw in with_password.into_iter().map(get_password) {
                op = op.with_password(pw?)?;
            }
            for mut stream in load_files(certs)? {
                op = op.with_certs(&C::from_reader(sop, &mut stream)?)?;
            }
            let session_key =
                op.plaintext(&mut io::stdin())?.to_writer(&mut io::stdout())?;

            if let Some(mut sko) = session_key_out {
                if let Some(sk) = session_key {
                    writeln!(sko, "{}", sk)?;
                } else {
                    return Err(Error::UnsupportedSubcommand.into());
                }
            }
        },

        Operation::Decrypt {
            session_key_out,
            with_session_key,
            with_password,
            verifications_out,
            verify_with,
            verify_not_before,
            verify_not_after,
            with_key_password,
            keys,
        } => {
            let session_key_out: Option<Box<dyn io::Write>> =
                if let Some(f) = session_key_out {
                    Some(Box::new(create_file(f)?))
                } else {
                    None
                };

            if verifications_out.is_none() != verify_with.is_empty() {
                return Err(anyhow::Error::from(Error::IncompleteVerification))
                    .context("--verifications-out and --verify-with \
                              must both be given");
            }

            let mut verify_out: Box<dyn io::Write> =
                if let Some(f) = verifications_out {
                    Box::new(create_file(f)?)
                } else {
                    Box::new(io::sink())
                };

            let mut op = sop.decrypt()?;

            for mut stream in load_files(with_session_key)? {
                let mut sk = String::new();
                stream.read_to_string(&mut sk)?;
                op = op.with_session_key(sk.parse()?)?;
            }

            for pw in with_password.into_iter().map(get_password_unchecked) {
                op = op.with_password(pw?)?;
            }

            for p in with_key_password {
                op = op.with_key_password(get_password_unchecked(p)?)?;
            }

            for mut stream in load_files(keys)? {
                op = op.with_keys(&K::from_reader(sop, &mut stream)?)?;
            }

            for mut stream in load_files(verify_with)? {
                op = op.verify_with_certs(&C::from_reader(sop, &mut stream)?)?;
            }

            if let Some(t) = verify_not_before {
                op = op.verify_not_before(t.into());
            }

            if let Some(t) = verify_not_after {
                op = op.verify_not_after(t.into());
            }

            let (session_key, verifications) =
                op.ciphertext(&mut io::stdin())?.to_writer(&mut io::stdout())?;

            for v in verifications {
                writeln!(verify_out, "{}", v)?;
            }
            if let Some(mut sko) = session_key_out {
                if let Some(sk) = session_key {
                    writeln!(sko, "{}", sk)?;
                } else {
                    return Err(Error::UnsupportedSubcommand.into());
                }
            }
        },

        Operation::Armor { label, } => {
            #[allow(deprecated)]
            let op = sop.armor()?.label(label);
            op.data(&mut io::stdin())?
                .to_writer(&mut io::stdout())?;
        },

        Operation::Dearmor {} => {
            let op = sop.dearmor()?;
            op.data(&mut io::stdin())?
                .to_writer(&mut io::stdout())?;
        },

        Operation::InlineDetach { no_armor, signatures_out, } => {
            let op = sop.inline_detach()?;
            let mut signatures_out = create_file(signatures_out)?;
            let signatures =
                op.message(&mut io::stdin())?.to_writer(&mut io::stdout())?;
            signatures.to_writer(! no_armor, &mut signatures_out)?;
        },

        Operation::InlineVerify {
            not_before,
            not_after,
            verifications_out,
            certs,
        } => {
            let mut op = sop.inline_verify()?;

            if let Some(t) = not_before {
                op = op.not_before(t.into());
            }
            if let Some(t) = not_after {
                op = op.not_after(t.into());
            }
            let mut verifications_out: Box<dyn io::Write> =
                if let Some(f) = verifications_out {
                    Box::new(create_file(f)?)
                } else {
                    Box::new(io::sink())
                };
            for mut stream in load_files(certs)? {
                op = op.certs(&C::from_reader(sop, &mut stream)?)?;
            }
            let verifications =
                op.message(&mut io::stdin())?.to_writer(&mut io::stdout())?;

            for v in verifications {
                writeln!(&mut verifications_out, "{}", v)?;
            }
        },

        Operation::InlineSign { no_armor, as_, with_key_password, keys, } => {
            let mut op = sop.inline_sign()?.mode(as_);
            if no_armor {
                op = op.no_armor();
            }
            for p in with_key_password {
                op = op.with_key_password(get_password_unchecked(p)?)?;
            }
            for mut stream in load_files(keys)? {
                op = op.keys(&K::from_reader(sop, &mut stream)?)?;
            }
            op.data(&mut io::stdin())?.to_writer(&mut io::stdout())?;
        },

        Operation::Unsupported(args) => {
            return Err(anyhow::Error::from(Error::UnsupportedSubcommand))
                .context(format!("Subcommand {} is not supported", args[0]));
        },
    }
    Ok(())
}

fn is_special_designator<S: AsRef<str>>(file: S) -> bool {
    file.as_ref().starts_with("@")
}

/// Loads the given (special) file.
fn load_file<S: AsRef<str>>(file: S)
                            -> Result<Box<dyn io::Read + Send + Sync>> {
    let f = file.as_ref();

    if is_special_designator(f) {
        if Path::new(f).exists() {
            return Err(anyhow::Error::from(Error::AmbiguousInput))
                .context(format!("File {:?} exists", f));
        }

        #[cfg(unix)]
        {
            if f.starts_with("@FD:")
                && f[4..].chars().all(|c| c.is_ascii_digit())
            {
                use std::os::unix::io::{RawFd, FromRawFd};

                let fd: RawFd = f[4..].parse()
                    .map_err(|_| Error::UnsupportedSpecialPrefix)?;
                let f = unsafe {
                    std::fs::File::from_raw_fd(fd)
                };
                return Ok(Box::new(f));
            }

            if f.starts_with("@ENV:") {
                use std::os::unix::ffi::OsStringExt;

                let key = &f[5..];
                let value = std::env::var_os(key)
                    .ok_or(Error::UnsupportedSpecialPrefix)?;
                // Prevent leak to child processes.
                std::env::remove_var(key);
                return Ok(Box::new(io::Cursor::new(value.into_vec())));
            }
        }

        #[cfg(windows)]
        {
            if f.starts_with("@ENV:") {
                let key = &f[5..];
                let value = std::env::var(key)
                    .map_err(|_| Error::UnsupportedSpecialPrefix)?;
                // Prevent leak to child processes.
                std::env::remove_var(key);
                return Ok(Box::new(io::Cursor::new(value.into_bytes())));
            }
        }

        return Err(anyhow::Error::from(Error::UnsupportedSpecialPrefix));
    }

    std::fs::File::open(f)
        .map(|f| -> Box<dyn io::Read + Send + Sync> { Box::new(f) })
        .map_err(|_| Error::MissingInput)
        .context(format!("Failed to open file {:?}", f))
}

/// Gets the password from the given location.
///
/// Use this for passwords given to create artifacts.
///
/// Passwords are indirect data types, meaning that they are
/// loaded from files or special designators.
fn get_password<S: AsRef<str>>(location: S) -> Result<Password> {
    let mut stream = load_file(location)?;
    let mut pw = Vec::new();
    stream.read_to_end(&mut pw)?;
    Ok(Password::new(pw)?)
}

/// Gets the password from the given location.
///
/// Use this for passwords given to consume artifacts.
///
/// Passwords are indirect data types, meaning that they are
/// loaded from files or special designators.
fn get_password_unchecked<S: AsRef<str>>(location: S) -> Result<Password> {
    let mut stream = load_file(location)?;
    let mut pw = Vec::new();
    stream.read_to_end(&mut pw)?;
    Ok(Password::new_unchecked(pw))
}

/// Creates the given (special) file.
fn create_file<S: AsRef<str>>(file: S) -> Result<std::fs::File> {
    let f = file.as_ref();

    if is_special_designator(f) {
        if Path::new(f).exists() {
            return Err(anyhow::Error::from(Error::AmbiguousInput))
                .context(format!("File {:?} exists", f));
        }

        #[cfg(unix)]
        {
            if f.starts_with("@FD:")
                && f[4..].chars().all(|c| c.is_ascii_digit())
            {
                use std::os::unix::io::{RawFd, FromRawFd};

                let fd: RawFd = f[4..].parse()
                    .map_err(|_| Error::UnsupportedSpecialPrefix)?;
                let f = unsafe {
                    std::fs::File::from_raw_fd(fd)
                };
                return Ok(f);
            }
        }

        return Err(anyhow::Error::from(Error::UnsupportedSpecialPrefix));
    }

    if Path::new(f).exists() {
        return Err(anyhow::Error::from(Error::OutputExists))
            .context(format!("File {:?} exists", f));
    }

    std::fs::File::create(f).map_err(|_| Error::MissingInput) // XXX
            .context(format!("Failed to create file {:?}", f))
}

/// Loads the the (special) files.
fn load_files(files: Vec<String>)
              -> Result<Vec<Box<dyn io::Read + Send + Sync>>> {
    files.iter().map(load_file).collect()
}

/// Parses the given string depicting a ISO 8601 timestamp, rounding down.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct NotBefore(DateTime<Utc>);

impl std::str::FromStr for NotBefore {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<NotBefore> {
        match s {
            // XXX: parse "-" to None once we figure out how to do that
            // with clap.
            "now" => Ok(Utc::now()),
            _ => parse_iso8601(s, NaiveTime::from_hms_opt(0, 0, 0).unwrap()),
        }.map(|t| NotBefore(t))
    }
}

impl From<NotBefore> for std::time::SystemTime {
    fn from(t: NotBefore) -> std::time::SystemTime {
        t.0.into()
    }
}

/// Parses the given string depicting a ISO 8601 timestamp, rounding up.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct NotAfter(DateTime<Utc>);

impl std::str::FromStr for NotAfter {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<NotAfter> {
        match s {
            // XXX: parse "-" to None once we figure out how to do that
            // with clap.
            "now" => Ok(Utc::now()),
            _ => parse_iso8601(s, NaiveTime::from_hms_opt(23, 59, 59).unwrap()),
        }.map(|t| NotAfter(t))
    }
}

impl From<NotAfter> for std::time::SystemTime {
    fn from(t: NotAfter) -> std::time::SystemTime {
        t.0.into()
    }
}

/// Parses the given string depicting a ISO 8601 timestamp.
fn parse_iso8601(s: &str, pad_date_with: chrono::NaiveTime)
                 -> Result<DateTime<Utc>>
{
    // If you modify this function this function, synchronize the
    // changes with the copy in sqv.rs!
    for f in &[
        "%Y-%m-%dT%H:%M:%S%#z",
        "%Y-%m-%dT%H:%M:%S",
        "%Y-%m-%dT%H:%M%#z",
        "%Y-%m-%dT%H:%M",
        "%Y-%m-%dT%H%#z",
        "%Y-%m-%dT%H",
        "%Y%m%dT%H%M%S%#z",
        "%Y%m%dT%H%M%S",
        "%Y%m%dT%H%M%#z",
        "%Y%m%dT%H%M",
        "%Y%m%dT%H%#z",
        "%Y%m%dT%H",
    ] {
        if f.ends_with("%#z") {
            if let Ok(d) = DateTime::parse_from_str(s, *f) {
                return Ok(d.into());
            }
        } else {
            if let Ok(d) = chrono::NaiveDateTime::parse_from_str(s, *f) {
                return Ok(DateTime::from_utc(d, Utc));
            }
        }
    }
    for f in &[
        "%Y-%m-%d",
        "%Y-%m",
        "%Y-%j",
        "%Y%m%d",
        "%Y%m",
        "%Y%j",
        "%Y",
    ] {
        if let Ok(d) = chrono::NaiveDate::parse_from_str(s, *f) {
            return Ok(DateTime::from_utc(d.and_time(pad_date_with), Utc));
        }
    }
    Err(anyhow::anyhow!("Malformed ISO8601 timestamp: {}", s))
}

#[test]
fn test_parse_iso8601() {
    let z = chrono::NaiveTime::from_hms_opt(0, 0, 0).unwrap();
    parse_iso8601("2017-03-04T13:25:35Z", z).unwrap();
    parse_iso8601("2017-03-04T13:25:35+08:30", z).unwrap();
    parse_iso8601("2017-03-04T13:25:35", z).unwrap();
    parse_iso8601("2017-03-04T13:25Z", z).unwrap();
    parse_iso8601("2017-03-04T13:25", z).unwrap();
    // parse_iso8601("2017-03-04T13Z", z).unwrap(); // XXX: chrono doesn't like
    // parse_iso8601("2017-03-04T13", z).unwrap(); // ditto
    parse_iso8601("2017-03-04", z).unwrap();
    // parse_iso8601("2017-03", z).unwrap(); // ditto
    parse_iso8601("2017-031", z).unwrap();
    parse_iso8601("20170304T132535Z", z).unwrap();
    parse_iso8601("20170304T132535+0830", z).unwrap();
    parse_iso8601("20170304T132535", z).unwrap();
    parse_iso8601("20170304T1325Z", z).unwrap();
    parse_iso8601("20170304T1325", z).unwrap();
    // parse_iso8601("20170304T13Z", z).unwrap(); // ditto
    // parse_iso8601("20170304T13", z).unwrap(); // ditto
    parse_iso8601("20170304", z).unwrap();
    // parse_iso8601("201703", z).unwrap(); // ditto
    parse_iso8601("2017031", z).unwrap();
    // parse_iso8601("2017", z).unwrap(); // ditto
}

/// Errors defined by the Stateless OpenPGP Command-Line Protocol.
#[derive(thiserror::Error, Debug)]
enum Error {
    /// No acceptable signatures found ("sop verify").
    #[error("No acceptable signatures found")]
    NoSignature,

    /// Asymmetric algorithm unsupported ("sop encrypt").
    #[error("Asymmetric algorithm unsupported")]
    UnsupportedAsymmetricAlgo,

    /// Certificate not encryption-capable (e.g., expired, revoked,
    /// unacceptable usage flags) ("sop encrypt").
    #[error("Certificate not encryption-capable")]
    CertCannotEncrypt,

    /// Key not signature-capable (e.g., expired, revoked,
    /// unacceptable usage flags) (`sop sign` and `sop encrypt` with
    /// `--sign-with`).
    #[error("Key not signing-capable")]
    KeyCannotSign,

    /// Missing required argument.
    #[error("Missing required argument")]
    MissingArg,

    /// Incomplete verification instructions ("sop decrypt").
    #[error("Incomplete verification instructions")]
    IncompleteVerification,

    /// Unable to decrypt ("sop decrypt").
    #[error("Unable to decrypt")]
    CannotDecrypt,

    /// Non-"UTF-8" or otherwise unreliable password ("sop encrypt").
    #[error("Non-UTF-8 or otherwise unreliable password")]
    PasswordNotHumanReadable,

    /// Unsupported option.
    #[error("Unsupported option")]
    UnsupportedOption,

    /// Invalid data type (no secret key where "KEY" expected, etc).
    #[error("Invalid data type")]
    BadData,

    /// Non-text input where text expected.
    #[error("Non-text input where text expected")]
    ExpectedText,

    /// Output file already exists.
    #[error("Output file already exists")]
    OutputExists,

    /// Input file does not exist.
    #[error("Input file does not exist")]
    MissingInput,

    /// A "KEY" input is protected (locked) with a password, and "sop" cannot
    /// unlock it.
    #[error("A KEY input is protected with a password")]
    KeyIsProtected,

    /// Unsupported subcommand.
    #[error("Unsupported subcommand")]
    UnsupportedSubcommand,

    /// An indirect parameter is a special designator (it starts with "@") but
    /// "sop" does not know how to handle the prefix.
    #[error("An indirect parameter is a special designator with unknown prefix")]
    UnsupportedSpecialPrefix,

    /// A indirect input parameter is a special designator (it starts with
    /// "@"), and a filename matching the designator is actually present.
    #[error("A indirect input parameter is a special designator matches file")]
    AmbiguousInput,

    /// Options were supplied that are incompatible with each other.
    #[error("Options were supplied that are incompatible with each other")]
    IncompatibleOptions,

    /// The requested profile is unsupported (sop generate-key, sop
    /// encrypt), or the indicated subcommand does not accept profiles
    /// (sop list-profiles).
    #[error("Profile not supported")]
    UnsupportedProfile,

    /// An I/O operation failed.
    #[error("I/O operation failed")]
    IoError(#[source] io::Error),
}

impl From<crate::Error> for Error {
    fn from(e: crate::Error) -> Self {
        use crate::Error as E;
        use Error as CE;
        match e {
            E::NoSignature => CE::NoSignature,
            E::UnsupportedAsymmetricAlgo => CE::UnsupportedAsymmetricAlgo,
            E::CertCannotEncrypt => CE::CertCannotEncrypt,
            E::KeyCannotSign => CE::KeyCannotSign,
            E::MissingArg => CE::MissingArg,
            E::IncompleteVerification => CE::IncompleteVerification,
            E::CannotDecrypt => CE::CannotDecrypt,
            E::PasswordNotHumanReadable => CE::PasswordNotHumanReadable,
            E::UnsupportedOption => CE::UnsupportedOption,
            E::BadData => CE::BadData,
            E::ExpectedText => CE::ExpectedText,
            E::OutputExists => CE::OutputExists,
            E::MissingInput => CE::MissingInput,
            E::KeyIsProtected => CE::KeyIsProtected,
            E::AmbiguousInput => CE::AmbiguousInput,
            E::IncompatibleOptions => CE::IncompatibleOptions,
            E::NotImplemented => CE::UnsupportedSubcommand,
            E::UnsupportedProfile => CE::UnsupportedProfile,
            E::IoError(e) => CE::IoError(e),
        }
    }
}

impl From<Error> for i32 {
    fn from(e: Error) -> Self {
        use Error::*;
        match e {
            NoSignature => 3,
            UnsupportedAsymmetricAlgo => 13,
            CertCannotEncrypt => 17,
            MissingArg => 19,
            IncompleteVerification => 23,
            CannotDecrypt => 29,
            PasswordNotHumanReadable => 31,
            UnsupportedOption => 37,
            BadData => 41,
            ExpectedText => 53,
            OutputExists => 59,
            MissingInput => 61,
            KeyIsProtected => 67,
            UnsupportedSubcommand => 69,
            UnsupportedSpecialPrefix => 71,
            AmbiguousInput => 73,
            KeyCannotSign => 79,
            IncompatibleOptions => 83,
            UnsupportedProfile => 89,
            IoError(_) => 1,
        }
    }
}

/// Prints the error and causes, if any.
fn print_error_chain(err: &anyhow::Error) {
    eprintln!("           {}", err);
    err.chain().skip(1).for_each(|cause| eprintln!("  because: {}", cause));
}
